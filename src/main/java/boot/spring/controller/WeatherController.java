package boot.spring.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMNode;
import org.apache.axis2.AxisFault;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import boot.spring.soap.GetSupportCity;
import boot.spring.soap.GetSupportCityResponse;
import boot.spring.soap.GetSupportDataSet;
import boot.spring.soap.GetSupportDataSetResponse;
import boot.spring.soap.GetSupportDataSetResult_type0;
import boot.spring.soap.GetSupportProvince;
import boot.spring.soap.GetSupportProvinceResponse;
import boot.spring.soap.GetWeatherbyCityName;
import boot.spring.soap.GetWeatherbyCityNameResponse;
import boot.spring.soap.WeatherWebServiceStub;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@Api(tags = "天气预报接口")
@Controller
public class WeatherController {
	
	
	
	@ApiOperation("获取城市天气预报")
	@RequestMapping(value="/getWeatherbyCityName/{cityname}",method = RequestMethod.GET)
	@ResponseBody
	public String[] getWeatherbyCityName(@PathVariable String cityname) throws Exception{
		WeatherWebServiceStub sub = new WeatherWebServiceStub();
		GetWeatherbyCityName weather = new GetWeatherbyCityName();
		weather.setTheCityName(cityname);
		GetWeatherbyCityNameResponse wps = sub.getWeatherbyCityName(weather);
		String[] wuhanweather = wps.localGetWeatherbyCityNameResult.localString;
		for (String wuhan : wuhanweather ) {
			System.out.println(wuhan);
		}
		return wuhanweather;
	}
	
	@ApiOperation("获取支持的城市列表")
	@RequestMapping(value="/getCitysbyProvince/{province}",method = RequestMethod.GET)
	@ResponseBody
	public String[] getCitysbyProvince(@PathVariable String province) throws Exception{
		WeatherWebServiceStub sub = new WeatherWebServiceStub();
		GetSupportCity city = new GetSupportCity();
		city.setByProvinceName(province);
		GetSupportCityResponse rsp = sub.getSupportCity(city);
		String[] data = rsp.localGetSupportCityResult.getString();
		for (String d : data) {
			System.out.println(d);
		}
		return data;
	}	
	
	@ApiOperation("获取所有支持的洲、省份和城市列表")
	@RequestMapping(value="/getSupportList",method = RequestMethod.GET)
	@ResponseBody
	public List<String> getSupportList() throws Exception{
		List<String> list = new ArrayList<>();
		WeatherWebServiceStub sub = new WeatherWebServiceStub();
		GetSupportDataSet set = new GetSupportDataSet();
		GetSupportDataSetResponse setpsp = sub.getSupportDataSet(set);
		GetSupportDataSetResult_type0 a = setpsp.getGetSupportDataSetResult();
		OMElement root = a.localExtraElement;
		Iterator iterator_layer1 = root.getChildElements();
		while (iterator_layer1.hasNext())
		{
			OMElement result = (OMElement) iterator_layer1.next();
			Iterator innerItr_layer2 = result.getChildElements();
			while (innerItr_layer2.hasNext())
			{
				OMElement result_2 = (OMElement) innerItr_layer2.next();
 
				Iterator innerItr_layer3 = result.getChildElements();
				{
 
					while (innerItr_layer3.hasNext())
					{
 
						OMElement result_l3 = (OMElement) innerItr_layer3.next();
						String s2 = result_l3.getLocalName();
						Iterator ss = result_l3.getChildElements();
						while (ss.hasNext()){
							OMElement result_l4 = (OMElement)ss.next();
							System.out.println(result_l4.getText());
							list.add(result_l4.getText());
						}
						
					}
				}
 
			}
		}
		return list;
	}		
	
	public static void main(String[] args) throws Exception {
		

	}
}
